<?php
namespace Gratin\Translatable;

use Illuminate\Support\ServiceProvider;

class TranslatableServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
    }
}
