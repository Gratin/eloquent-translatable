<?php
declare(strict_types=1);

namespace Gratin\Translatable\Traits;

use Illuminate\Http\Request;
use Gratin\Translatable\Translatable;

trait TranslatableTrait
{
    /**
     * Retrieves translations for the associated model
     * @param string $locale
     * @param bool #flattenDeep Using relation Strategy wether we should sub-group by field name
     * @return array<[ locale => [] ]>
     */
    public function translations(?string $locale = null, ?bool $flattenDeep = false)
    {
        switch ($this->translationStrategy) {
            case Translatable::SUFFIX:
                $translations = $this->makeTranslations(Translatable::SUFFIX);
                break;
            case Translatable::PREFIX:
                $translations = $this->makeTranslations(Translatable::PREFIX);
                break;
            case Translatable::RELATION:
                $translations = self::select($this->translationTable.'.*')->join(
                    $this->translationTable,
                    $this->table.'.id',
                    $this->translationTable.'.source_id'
                )->get()->groupBy('locale')->transform(function ($element, $i) use ($flattenDeep) {
                    return $element->groupBy('attribute')->transform(function ($el, $k) use ($flattenDeep) {
                        if ($flattenDeep) {
                            return $el->first();
                        }
                        return $el;
                    });
                });
                break;
            default:
                $strategy = Translatable::get($this->translationStrategy);
                $handler = Translatable::handler($this->translationStrategy);
                $translations = call_user_func_array($handler, [$this]);
                break;
        }

        if ($locale) {
            return $translations[$locale];
        }

        return $translations;
    }

    /**
     * Retrieves translation for the associated model for a single locale
     * @param ?string $locale The locale to find
     * @return array<[ locale => [] ]>
     */
    public function translation(?string $locale = null)
    {
        if (!$locale) {
            $locale = Request::server('HTTP_ACCEPT_LANGUAGE');
        }

        switch ($this->translationStrategy) {
            case Translatable::SUFFIX:
                $translations = $this->makeTranslations(Translatable::SUFFIX);
                $translations = $translations[$locale] ?? [];
                break;
            case Translatable::PREFIX:
                $translations = $this->makeTranslations(Translatable::PREFIX);
                $translations = $translations[$locale] ?? [];
                break;
            case Translatable::RELATION:
                $translations = self::select($this->translationTable.'.*')->join(
                    $this->translationTable,
                    $this->table.'.id',
                    $this->translationTable.'.source_id'
                )->where($this->translationTable.'.locale', $locale)->get();
                break;
            default:
                break;
        }

        return $translations;
    }

    /**
     * Retrieves translation for the associated model for a single locale
     * @param string $attribute
     * @param ?string $locale The locale to find
     * @return array<[ locale => [] ]>
     */
    public function translate(string $attribute, ?string $locale = null)
    {
        if (!$locale) {
            $locale = request()->server('HTTP_ACCEPT_LANGUAGE');
        }

        switch ($this->translationStrategy) {
            case Translatable::SUFFIX:
                return $this->attributes[$attribute.Translatable::$delimiter.$locale] ?? null;
                break;
            case Translatable::PREFIX:
                return $this->attributes[$locale.Translatable::$delimiter.$attribute] ?? null;
                break;
            case Translatable::RELATION:
                return $translations = self::select($this->translationTable.'.*')->join(
                    $this->translationTable,
                    $this->table.'.id',
                    $this->translationTable.'.source_id'
                )
                ->where($this->translationTable.'.locale', $locale)
                ->where($this->translationTable.'.attribute', $attribute)
                ->get();
                break;
            default:
                break;
        }
    }

    /**
     * Parses attributes name using a pattern
     * @param string The type of pattern to use
     * @return array<[locale=>[]]>
     */
    private function makeTranslations(string $type)
    {
        $translations = [];

        switch ($type) {
            case Translatable::SUFFIX:
                foreach ($this->getAttributes() as $key => $value) {
                    preg_match('/([\w]+)'.Translatable::$delimiter.'([a-zA-Z]+)/', $key, $matches);
                    if (!empty($matches)) {
                        // [name, $locale]
                        if (!array_key_exists($matches[2], $translations)) {
                            $translations[$matches[2]] = [];
                        }

                        $translations[$matches[2]][$matches[1]] = $value;
                    }
                }
                break;
            case Translatable::PREFIX:
                foreach ($this->getAttributes() as $key => $value) {
                    preg_match('/([a-zA-Z]+)'.Translatable::$delimiter.'([\w]+)/', $key, $matches);
                    if (!empty($matches)) {
                        // [name, $locale]
                        if (!array_key_exists($matches[1], $translations)) {
                            $translations[$matches[1]] = [];
                        }

                        $translations[$matches[1]][$matches[2]] = $value;
                    }
                }
                break;
        }

        return $translations;
    }

    public function __get($attribute)
    {
        if (isset($this->translationStrategy)) {
            switch ($this->translationStrategy) {
                case Translatable::SUFFIX:
                    preg_match('/([\w]+)'.Translatable::$delimiter.'([a-zA-Z]+)/', $key, $matches);
                    if (count($matches) > 2) {
                        $translation = $this->translate($attribute, $maches[2]);
                        return $translation;
                    } else {
                        return $this->translate($attribute, \App::getLocale());
                    }
                    return $this->translate($attribute);
                    break;
                case Translatable::PREFIX:
                    preg_match('/([a-zA-Z]+)'.Translatable::$delimiter.'([\w]+)/', $key, $matches);
                    if (count($matches) > 2) {
                        $translation = $this->translate($attribute, $matches[1]);
                        return $translation;
                    } else {
                        return $this->translate($attribute, \App::getLocale());
                    }
                    return $this->translate($attribute);
                    break;
                case Translatable::RELATION:
                    return $this->translate($attribute);
                    break;
            }
        }

        return $attribute;
    }
}
