<?php
namespace Gratin\Translatable;

class Translatable
{
    public const SUFFIX = "suffix";
    public const PREFIX = "prefix";
    public const RELATION = "relation";

    public const PATTERNS = [

    ];
 
    public static $strategies = [];
    public static $handlers = [];

    public static $delimiter = '__';

    /**
     * Creates a strategy
     * @param string $name The name of the strategy
     * @param string $value The string reprensetation of the strategy
     * @param Callable $callback The callback used for the given strategy
     *
     * @return void
     */
    public static function defineStrategy(string $name, string $value, callable $callback): void
    {
        self::$strategies[strtoupper($name)]    = $value;
        self::$handlers[strtoupper($name)]      = $callback;
    }

    /**
     * Gets a strategy by its name
     * @param string $name The strategy name
     * @throws InvalidArgumentException If strategy does not exists
     * @return string
     */
    public static function get(string $name)
    {
        $name = strtoupper($name);
        if (defined($name)) {
            return self::$name;
        }

        if (array_key_exists($name, self::$strategies)) {
            return self::$strategies[$name];
        }

        throw new \InvalidArgumentException($name. ' strategy not defined');
    }

    /**
     * Retrieves the callback handler for a given strategy
     * @param string $name The strategy name
     * @throws InvalidArgumentException If the handler does not exists
     * @return Callable
     */
    public static function handler(string $name): callable
    {
        $name = strtoupper($name);
        if (array_key_exists($name, self::$handlers)) {
            return self::$handlers[$name];
        }

        throw new \InvalidArgumentException($name. ' handler not defined');
    }
}
