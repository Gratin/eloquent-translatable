# Eloquent Translatable Trait

* Definition
* Usage

## Definition

Simple package that lets you localize your eloquent models easily.  
The trait supports three strategies:  

*   Relation
*   Suffix
*   Prefix

All strategies have a corresponding constant inside `Gratin\Translatable\Translatable`.  
Your eloquent model must have this variable set:

* `public $translationStrategy` : The translation type to use.

## Adding Strategies

You can add Strategies to the Translatable (such as JSON) by doing like this:
`Translatable::defineStrategy(string $name, string $value, Callable $callback)`
In which the `$name` is the name of the strategy, the `$value` is the string representation and the `$callback` is
the callback function that will handle the mapping. The model is passed as the argument to the callback.
The callback should return a `Collection`.

## Strategies

### Relation

First you need to import the trait `Gratin\Translatable\Traits\TranslatableTrait`  
Another variable needs to be defined in your eloquent model:  

* `public $translationTable` : The name of the table used for translations.

#### The translation table

The translation table's schema should look like this:

`int id`, `int source_id`, `string locale`, `string attribute`, `string value`

### Suffix

Uses the delimiter `__` to guess the locales of the columns.

### Prefix

Uses the delimiter `__` to guess the locales of the columns.

## Overriding the delimiter
You can set the `$delimiter` property of the Translatable class.

## Usage

The trait comes with three functions:

* `translations()`
Returns all the translations for a given model, split by locales

* `translation(?string $locale = null)`
Returns the translation for a single locale

* `translate(string $attribute, ?string $locale = null)`
Returns the translation for a specific field for a given locale

### translations()

Returns all translated fields in an array grouped by locale `['en'=>['id'=>1, '{fieldname}'=>[]]]`