<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 't',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
$capsule->bootEloquent();

class Model extends \Illuminate\Database\Eloquent\Model
{
    use \Gratin\Translatable\Traits\TranslatableTrait;

    protected $table = "a";
    public $translationTable = "b";
    public $translationStrategy = \Gratin\Translatable\Translatable::PREFIX;

}

class Strategy
{
    public static function strategyTest(\Illuminate\Database\Eloquent\Model $model)
    {
        return ['a'];
    }
}

class ModelStrategy extends \Illuminate\Database\Eloquent\Model
{
    use \Gratin\Translatable\Traits\TranslatableTrait;

    protected $table = "a";
    public $translationTable = "b";
    public $translationStrategy = "test";
}
use PHPUnit\Framework\TestCase;

class TranslationTests extends TestCase
{
    public function testTranslations()
    {
        // \Gratin\Translatable\Translatable::defineStrategy('TEST', 'test', [Strategy::class, 'strategyTest']);
        $a = Model::find(1);
        $t = $a->translations(true);
        print_r($t);die;
        $this->assertTrue(true);
    }

    // public function testTranslation()
    // {
    //     $a = Model::find(1);
    //     $t = $a->translation('fr');
    //     $this->assertTrue(true);
    // }

    // public function testTranslate()
    // {
    //     $a = Model::find(1);
    //     $t = $a->translate('title', 'fr');
    //     print_r($t);die;
    //     $this->assertTrue(true);
    // }
}